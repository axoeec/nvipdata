
# National Vulnerability Intelligence Platform (NVIP) - Project Data

This repo contains the data needed for the National Vulnerability Intelligence Platform (NVIP) project.

## Repo Index
- CVE data pulled from NVD, MITRE and CNNVD is at nvd-cve.csv, mitre-cve.csv, and china-cve.csv files. 
- Training data for NVIP can be found at /characterization.
- CVE details for MITRE and CNNVD are at mitre-cve/ and china-cve/. 

# How to Use
You can clone this repo and configure NVIP data path in the nvip.properties to use it in the NVIP Project.
For more details about the NVIP project, please take a look at https://bitbucket.org/axoeec/nvip/src/master/README.md.
